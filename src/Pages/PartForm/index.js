import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import Template from "../../Components/Template";
import {
  Title,
  ContainerForm,
  Form,
  DivShowErrors,
  LabelShowErrors,
  ContainerButton,
  Button
} from "./styles";
import { Input } from "../../Components/InputText";
import ValidarRegras from "../../Utils/ValidarRegras";

import { savePart, getPart } from "../../Firebase/part";

class PartForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: undefined,
      formulario: {
        nome: {
          name: "nome",
          title: "Nome",
          valor: "",
          valido: true,
          msgErro: "",
          regras: {
            required: true
          }
        },
        sobrenome: {
          name: "sobrenome",
          title: "Sobrenome",
          valor: "",
          valido: true,
          msgErro: "",
          regras: {
            required: true
          }
        },
        email: {
          name: "email",
          title: "Email",
          valor: "",
          valido: true,
          msgErro: "",
          regras: {
            required: true,
            isEmail: true
          }
        },
        cpf: {
          name: "cpf",
          title: "CPF",
          valor: "",
          valido: true,
          msgErro: "",
          regras: {
            required: true,
            isCpf: true
          }
        },
        telefone: {
          name: "telefone",
          title: "Telefone",
          valor: "",
          valido: true,
          msgErro: "",
          regras: {
            required: true
          }
        }
      },
      showErrors: true
    };
  }

  componentDidMount() {
    this._isMounted = true;
    const { params } = this.props.match;
    let partID = params.partID;
    if (partID !== undefined) {
      getPart(partID, this.cbGetPart);
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  cbGetPart = part => {
    let copia = { ...this.state.formulario };
    copia.nome.valido = true;
    copia.nome.valor = part.nome;
    copia.nome.msgErro = "";

    copia.sobrenome.valido = true;
    copia.sobrenome.valor = part.sobrenome;
    copia.sobrenome.msgErro = "";

    copia.email.valido = true;
    copia.email.valor = part.email;
    copia.email.msgErro = "";

    copia.cpf.valido = true;
    copia.cpf.valor = part.cpf;
    copia.cpf.msgErro = "";

    copia.telefone.valido = true;
    copia.telefone.valor = part.telefone;
    copia.telefone.msgErro = "";

    if (this._isMounted) {
      this.setState({
        showErrors: false,
        formulario: copia,
        id: part.id
      });
    }
  };

  changeText = (value, item) => {
    let copia = { ...this.state.formulario };
    copia[item.name].valor = value;
    let atributo = ValidarRegras(copia[item.name], copia);
    copia[item.name] = atributo;
    if (this._isMounted) {
      this.setState({
        formulario: copia,
        showErrors: false
      });
    }
  };

  showErrosWithField = (atributo, showErrors) =>
    !atributo.valido && showErrors ? (
      <DivShowErrors>
        <LabelShowErrors>{atributo.msgErro}</LabelShowErrors>
      </DivShowErrors>
    ) : null;

  save = () => {
    let copia = this.state.formulario;
    let formValido = true;
    let form = [];
    for (let key in copia) {
      let atributo = ValidarRegras(copia[key], copia);
      copia[key] = atributo;
      formValido = atributo.valido ? formValido : false;
      form[key] =
        atributo.valor.value !== undefined
          ? atributo.valor.value
          : atributo.valor;
    }
    if (this._isMounted) {
      this.setState({
        formulario: copia,
        showErrors: true
      });
    }
    if (formValido) {
      if (this.state.id !== undefined) {
        form["id"] = this.state.id;
      }
      savePart(form);

      this.props.history.push("/parts");
    }
  };

  cancelar = () => {
    this.props.history.push("/parts");
  }

  render() {
    const { formulario, showErrors } = this.state;
    return (
      <Template {...this.props}>
        <Title>Cadastrar/ Editar Parte</Title>
        <ContainerForm>
          <Form>
            <Input
              id="standard-dense"
              label={formulario.nome.title}
              value={formulario.nome.valor}
              onChange={e => this.changeText(e.target.value, formulario.nome)}
            />
            {this.showErrosWithField(formulario.nome, showErrors)}
            <Input
              id="standard-dense"
              label={formulario.sobrenome.title}
              value={formulario.sobrenome.valor}
              onChange={e =>
                this.changeText(e.target.value, formulario.sobrenome)
              }
            />
            {this.showErrosWithField(formulario.sobrenome, showErrors)}
            <Input
              id="standard-dense"
              label={formulario.email.title}
              value={formulario.email.valor}
              onChange={e => this.changeText(e.target.value, formulario.email)}
            />
            {this.showErrosWithField(formulario.email, showErrors)}
            <Input
              id="standard-dense"
              label={formulario.cpf.title}
              value={formulario.cpf.valor}
              onChange={e => this.changeText(e.target.value, formulario.cpf)}
            />
            {this.showErrosWithField(formulario.cpf, showErrors)}
            <Input
              id="standard-dense"
              label={formulario.telefone.title}
              value={formulario.telefone.valor}
              onChange={e =>
                this.changeText(e.target.value, formulario.telefone)
              }
            />
            {this.showErrosWithField(formulario.telefone, showErrors)}

            <ContainerButton>
              <Button onClick={this.cancelar}>Cancelar</Button>
              <Button onClick={this.save}>Salvar</Button>
            </ContainerButton>
          </Form>
        </ContainerForm>
      </Template>
    );
  }
}

export default withRouter(PartForm);
