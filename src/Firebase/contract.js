import firebase from "firebase";
import { CONTRACTS } from "./const";
import { doUploadBase64WithPath } from './photos';

export const saveContract = contract => {
  let ref = firebase.firestore().collection(CONTRACTS);
  let batch = firebase.firestore().batch();
  let photo = contract.photo;
  let id = contract.id === undefined ? ref.doc().id : contract.id;
  batch.set(ref.doc(id), { id, ...contract });
  if(photo.object) {
    delete contract.photo;
    let photoSave = {};
    let refStorage = `${CONTRACTS}/${id}/photo/${id}`;
    let refContract = `${CONTRACTS}/${id}`;
    photoSave.url = "";
    photoSave.name = photo.name;
    photoSave.id = id;
    doUploadBase64WithPath(photo.object, refStorage, refContract,photoSave);
  }
  batch.commit();
};



export const listContracts = cb => {
  return firebase
    .firestore()
    .collection(CONTRACTS)
    .onSnapshot(snapshot => {
      let contracts = [];
      snapshot.forEach(snap => {
        contracts.push(snap.data());
      });
      cb(contracts);
    });
};

export const getContract = (id,cb) => {
    firebase
    .firestore()
    .collection(CONTRACTS)
    .where("id","==",id)
    .onSnapshot(snapshot => {
      let contract = {};
      snapshot.forEach(snap => {
        contract = snap.data();
      });
      cb(contract);
    });
}

export const deleteContract = (id) => {
    firebase
    .firestore()
    .collection(CONTRACTS)
    .doc(id)
    .delete()
}
