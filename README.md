Para rodar o projeto é necessário ter o node.js instalado na máquina
### `https://nodejs.org/`

caso já tenha instalado o node navegar até a raiz do projeto através do terminal
e executar o comando 
### `npm install`


Após instalar todas as dependências você poderá executar o projeto
### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
