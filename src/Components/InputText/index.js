import React from 'react';
import { Container, InputText } from './styles';

export const Input = (props) => {
    return (
        <Container>
            <InputText {...props} />
        </Container>
    )
}