import React, { Component } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Contracts from "./Pages/Contract";
import ContractForm from "./Pages/ContractForm";
import PartForm from "./Pages/PartForm";
import Parts from "./Pages/Parts";

class Router extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route path="/contracts" render={() => <Contracts />} />
          <Route path="/parts" render={() => <Parts />} />
          <Route
            path="/contract-form/:contractID"
            render={() => <ContractForm />}
          />
          <Route path="/contract-form" render={() => <ContractForm />} />
          <Route path="/part-form/:partID" render={() => <PartForm />} />
          <Route path="/part-form" render={() => <PartForm />} />
          <Route path="/" render={() => <Contracts />} />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default Router;
