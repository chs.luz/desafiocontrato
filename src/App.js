import React from "react";
import GlobalStyle from "./styles";
import Routes from "./Routes";
import firebase from 'firebase';
import { config } from './config';

firebase.initializeApp(config)

function App() {
  return (
    <>
      <Routes />
      <GlobalStyle />
    </>
  );
}

export default App;
