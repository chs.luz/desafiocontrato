const validar = (atributo, formulario) => {
  let regras = atributo.regras;
  for (let regra in regras) {
    switch (regra) {
      case "required": {
        if (atributo.regras[regra] === false) {
          atributo.valido = true;
          atributo.msgErro = "";
          break;
        }
        let valido = validarRequired(atributo.valor);
        if (!valido) {
          atributo.valido = false;
          atributo.msgErro = nameForError(atributo) + " é obrigatório";
        } else {
          atributo.valido = true;
          atributo.msgErro = "";
        }
        break;
      }
      case "isEmail": {
        let valido = validarEmail(atributo.valor);
        if (!valido) {
          atributo.valido = false;
          atributo.msgErro = nameForError(atributo) + " inválido";
        } else {
          atributo.valido = true;
          atributo.msgErro = "";
        }
        break;
      }
      case "isCpf": {
        let valido = validarCpf(atributo.valor);
        if (!valido) {
          atributo.valido = false;
          atributo.msgErro = nameForError(atributo) + " inválido";
        } else {
          atributo.valido = true;
          atributo.msgErro = "";
        }
        break;
      }
      case "minLength": {
        let valido = validarMinLength(atributo.valor, regras[regra]);
        if (!valido) {
          atributo.valido = false;
          atributo.msgErro =
            atributo.nome +
            " deve conter no mínimo " +
            regras[regra] +
            " digitos";
        } else {
          atributo.valido = true;
          atributo.msgErro = "";
        }
        break;
      }
      case "confirmarSenha": {
        let valido = validarConfirmarSenha(
          atributo.valor,
          formulario[regras.confirmarSenha].valor
        );
        if (!valido) {
          atributo.valido = false;
          atributo.msgErro = "Senhas não conferem";
        } else {
          atributo.valido = true;
          atributo.msgErro = "";
        }
        break;
      }
      case "checkbox": {
        atributo.valido = true;
        atributo.msgErro = "";
        break;
      }
      default: {
        return atributo;
      }
    }
  }
  return atributo;
};


const nameForError = (atributo) => {
  let name = '';
  name = atributo.nome ? atributo.nome : name;
  name = atributo.title ? atributo.title : name;
  name = atributo.placeholder ? atributo.placeholder : name;
  return name
}
const validarRequired = valor => {
  if (valor.trim() !== "" && (valor === undefined || valor.length > 0 || valor > 0 || valor.value !== undefined)) {
    return true;
  } else {
    return false;
  }
};

const validarEmail = valor => {
  const expression = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return expression.test(String(valor).toLowerCase());
};

const validarCpf = strCPF => {
  strCPF = strCPF
    .replace(".", "")
    .replace(".", "")
    .replace("-", "");
  var Soma;
  var Resto;
  Soma = 0;
  if (strCPF === "00000000000") return false;

  for (let i = 1; i <= 9; i++)
    Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
  Resto = (Soma * 10) % 11;

  if (Resto === 10 || Resto === 11) Resto = 0;
  if (Resto !== parseInt(strCPF.substring(9, 10))) return false;

  Soma = 0;
  for (let i = 1; i <= 10; i++)
    Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
  Resto = (Soma * 10) % 11;

  if (Resto === 10 || Resto === 11) Resto = 0;
  if (Resto !== parseInt(strCPF.substring(10, 11))) return false;
  return true;
};

const validarMinLength = (valor, valorRegra) => {
  if (valor.length >= valorRegra) {
    return true;
  } else {
    return false;
  }
};

const validarConfirmarSenha = (valor, senha) => {
  if (valor === senha) {
    return true;
  } else {
    return false;
  }
};

export default validar;
