import styled from "styled-components";

export const ContainerForm = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  width: 90%;
  margin-left: 30px;
  margin-right: 30px;
`;

export const ButtomNewContract = styled.button `
    background: #0000ff;
    color: #ffffff;
    height: 30px;
    width: 80px;
    position: absolute;
    font-size: 16px;
    right: 30px;
    top: 20px;
    border-radius: 10px;

    &:hover {
      background: #0000a0;
    }
`

export const Title = styled.div`
  display: flex;
  flex-direction: column;
  height: 60px;
  width: 100%;
  align-items: center;
  justify-content: center;
  font-size: 16px;
  font-weight: bold;
`;

export const TdTitle = styled.td `
    border: 1px solid #000000;
    text-align: center;
    align-items:center;
    background: #74b3b6;
    height: 30px;
`

export const TdContent = styled.td `
    border: 1px solid;
    text-align: center;
    align-items:center;
    height: 40px;
    color: gray;
`

export const IconContainer = styled.div `
 display: flex;
 justify-content: center;
`

export const Icon = styled.div `
    padding: 5px;

    &:hover {
      color: red;
    }
`
