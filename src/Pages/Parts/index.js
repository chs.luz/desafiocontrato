import React, { Component } from "react";
import Template from "../../Components/Template";
import { withRouter } from "react-router-dom";
import {
  ContainerForm,
  Title,
  IconContainer,
  Icon,
  TdTitle,
  TdContent,
  ButtomNewPart
} from "./styles";
import { MdRemove, MdEdit } from "react-icons/md";
import { listParts, deletePart } from "../../Firebase/part";

class Parts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      itens: []
    };
  }

  componentDidMount() {
    this._isMounted = true;
    this.listenerParts = listParts(this.cbListParts);
  }

  componentWillUnmount() {
    this._isMounted = false;
    this.listenerParts();
  }

  cbListParts = list => {
    if (this._isMounted) {
      this.setState({
        itens: list
      });
    }
  };

  createNewPart = () => {
    this.props.history.push("/part-form");
  };

  delete = id => {
    deletePart(id);
  };

  editPart = id => {
    this.props.history.push("/part-form/" + id);
  };

  render() {
    return (
      <Template {...this.props}>
        <ContainerForm>
          <ButtomNewPart onClick={this.createNewPart}>Novo</ButtomNewPart>
          <Title>Partes</Title>
          <table>
            <thead>
              <tr>
                <TdTitle>ID</TdTitle>
                <TdTitle>Nome</TdTitle>
                <TdTitle>Sobrenome</TdTitle>
                <TdTitle>Email</TdTitle>
                <TdTitle>Cpf</TdTitle>
                <TdTitle>Telefone</TdTitle>
                <TdTitle>Ações</TdTitle>
              </tr>
            </thead>
            <tbody>
              {this.state.itens.length > 0 ? (
                this.state.itens.map(item => (
                  <tr key={item.id}>
                    <TdContent>{item.id}</TdContent>
                    <TdContent>{item.nome}</TdContent>
                    <TdContent>{item.sobrenome}</TdContent>
                    <TdContent>{item.email}</TdContent>
                    <TdContent>{item.cpf}</TdContent>
                    <TdContent>{item.telefone}</TdContent>
                    <TdContent>
                      <IconContainer>
                        <Icon>
                          <MdEdit
                            onClick={() => this.editPart(item.id)}
                            size={20}
                            color={"blue"}
                          />
                        </Icon>
                        {/* <Icon>
                        <MdRemove onClick={() => this.delete(item.id)} size={20} color={"blue"} />
                     </Icon>*/}
                      </IconContainer>
                    </TdContent>
                  </tr>
                ))
              ) : (
                <tr>
                  <TdContent colSpan={7}>Nenhum Resultado Encontrado</TdContent>
                </tr>
              )}
            </tbody>
          </table>
        </ContainerForm>
      </Template>
    );
  }
}

export default withRouter(Parts);
