import React from "react";
import { Container, Title, Menu, MenuItem } from './styles';


const redirectTo = (props, to) => {
  props.history.push(to);
}


const Header = props => {
  return (
    <Container>
        <Title>Desafio Contrato</Title>
        <Menu>
          <MenuItem onClick={() => redirectTo(props,'/contracts')}>Contratos</MenuItem>
          <MenuItem onClick={() => redirectTo(props,'/parts')}>Partes</MenuItem>
        </Menu>
    </Container>
  );
};

export default Header;
