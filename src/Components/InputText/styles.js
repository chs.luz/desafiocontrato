import styled from "styled-components";
import { TextField } from '@material-ui/core';


export const Container = styled.div `
 display: flex;
 width: 100%;
 flex-direction: column;
 padding-top: 10px;
 align-items: center;
`

export const InputText = styled(TextField) `
 width: 90%;
 padding-left: 10px;
 background-color: lightgray;
`