import firebase from "firebase";

export const doUploadBase64WithPath = (base64, bucket, ref, photo) => {
  firebase
    .storage()
    .ref()
    .child(bucket)
    .putString(base64, "data_url")
    .then(snap => {
      snap.ref.getDownloadURL().then(url => {
        photo.url = url;
        firebase
          .firestore()
          .doc(ref)
          .update({ photo });
      });
    });
};
