import styled from "styled-components";
import Modal from "react-modal";

export const Container = styled(Modal)`
  width: 80%;
  height: 100%;
`;

export const ContainerButton = styled.div`
  width: 100%;
  height: 40px;
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
`;

export const ButtomClose = styled.button`
  width: 40px;
  height: 40px;
  padding-right: 20px;
  background: blue;
  color: white;
`;
