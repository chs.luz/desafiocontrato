import React, { Component } from "react";
import {
  ContainerForm,
  Form,
  Content,
  ContainerFileUpload,
  TdContent,
  TdTitle
} from "./styles";
import { Input } from "../../Components/InputText";

class ViewContract extends Component {
  constructor(props) {
    super(props);
    this.state = {
      titulo: "",
      dataInicio: "",
      dataVencimento: "",
      parts: [],
      photo: { name: "", url: "" }
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ ...nextProps });
  }

  render() {
    const { titulo, dataInicio, dataVencimento, parts, photo } = this.state;
    return (
      <ContainerForm>
        <Form>
          <Input
            disabled={true}
            id="standard-dense"
            label="Nome"
            value={titulo}
          />
          <Input
            type="date"
            disabled={true}
            id="standard-dense"
            label="Data de Início"
            value={dataInicio}
          />
          <Input
            type="date"
            disabled={true}
            id="standard-dense"
            label="Data de Vencimento"
            value={dataVencimento}
          />
          <table style={{ width: "95%", marginTop: 20, marginLeft: 10 }}>
            <thead>
              <tr>
                <TdTitle>Nome</TdTitle>
                <TdTitle>Sobrenome</TdTitle>
                <TdTitle>Email</TdTitle>
              </tr>
            </thead>
            <tbody>
              {parts.map(part => (
                <tr key={part.id}>
                  <TdContent>{part.nome}</TdContent>
                  <TdContent>{part.sobrenome}</TdContent>
                  <TdContent>{part.email}</TdContent>
                </tr>
              ))}
            </tbody>
          </table>
          <ContainerFileUpload>
            <Content>
              {photo.name !== "" ? (
                <div style={{ textAlign: "center", marginTop: 10 }}>
                  <a href={photo.url} target="_blank" rel="noopener noreferrer">
                    {photo.name}
                  </a>
                </div>
              ) : null}
            </Content>
          </ContainerFileUpload>
        </Form>
      </ContainerForm>
    );
  }
}

export default ViewContract;
