import firebase from "firebase";
import { PARTS } from "./const";

export const savePart = part => {
  let ref = firebase.firestore().collection(PARTS);
  let id = part.id === undefined ? ref.doc().id : part.id;
  ref.doc(id).set({ id, ...part });
};

export const listParts = cb => {
  return firebase
    .firestore()
    .collection(PARTS)
    .orderBy("nome","asc")
    .onSnapshot(snapshot => {
      let parts = [];
      snapshot.forEach(snap => {
        parts.push(snap.data());
      });
      cb(parts);
    });
};

export const getPart = (id,cb) => {
    firebase
    .firestore()
    .collection(PARTS)
    .where("id","==",id)
    .onSnapshot(snapshot => {
      let part = {};
      snapshot.forEach(snap => {
        part = snap.data();
      });
      cb(part);
    });
}

export const deletePart = (id) => {
    firebase
    .firestore()
    .collection(PARTS)
    .doc(id)
    .delete()
}
