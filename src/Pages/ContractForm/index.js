import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import Template from "../../Components/Template";
import {
  Title,
  ContainerForm,
  Form,
  DivShowErrors,
  LabelShowErrors,
  ContainerButton,
  Button,
  Content,
  ContainerFileUpload,
  TdContent,
  TdTitle,
  Icon,
  IconContainer
} from "./styles";
import { Input } from "../../Components/InputText";
import ValidarRegras from "../../Utils/ValidarRegras";
import { saveContract, getContract } from "../../Firebase/contract";
import Upload from "../../Components/Upload";
import { listParts } from "../../Firebase/part";

class ContractForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: undefined,
      formulario: {
        titulo: {
          name: "titulo",
          title: "Título",
          valor: "",
          valido: true,
          msgErro: "",
          regras: {
            required: true
          }
        },
        dataInicio: {
          name: "dataInicio",
          title: "Data de Início",
          valor: " ",
          valido: true,
          msgErro: "",
          regras: {
            required: true
          }
        },
        dataVencimento: {
          name: "dataVencimento",
          title: "Data de Vencimento",
          valor: " ",
          valido: true,
          msgErro: "",
          regras: {
            required: true
          }
        }
      },
      file: {
        url: "",
        name: ""
      },
      parts: [],
      partsSelect: [],
      showErrors: false,
      showErroParts: false
    };
  }

  componentDidMount() {
    this._isMounted = true;
    const { params } = this.props.match;
    let contractID = params.contractID;
    if (contractID !== undefined) {
      getContract(contractID, this.cbGetContract);
    }
    this.listenerParts = listParts(this.cbListParts);
  }

  componentWillUnmount() {
    this._isMounted = false;
    this.listenerParts();
  }

  cbListParts = list => {
    let parts = list.map(l => {
      let select = this.state.partsSelect ? this.state.partsSelect : [];
      let exist = select.filter(partId => l.id === partId);
      return { ...l, checked: exist.length > 0 };
    });
    if (this._isMounted) {
      this.setState({
        parts
      });
    }
  };

  cbGetContract = contract => {
    let copia = { ...this.state.formulario };
    let file = this.state.file;
    copia.titulo.valido = true;
    copia.titulo.valor = contract.titulo;
    copia.titulo.msgErro = "";

    copia.dataInicio.valido = true;
    copia.dataInicio.valor = contract.dataInicio;
    copia.dataInicio.msgErro = "";

    copia.dataVencimento.valido = true;
    copia.dataVencimento.valor = contract.dataVencimento;
    copia.dataVencimento.msgErro = "";

    file.url = contract.photo ? contract.photo.url : "";
    file.name = contract.photo ? contract.photo.name : "";

    if (this._isMounted) {
      this.setState({
        showErrors: false,
        formulario: copia,
        id: contract.id,
        partsSelect: contract.parts,
        file
      });
    }
  };

  changeText = (value, item) => {
    let copia = { ...this.state.formulario };
    copia[item.name].valor = value;
    let atributo = ValidarRegras(copia[item.name], copia);
    copia[item.name] = atributo;
    if (this._isMounted) {
      this.setState({
        formulario: copia,
        showErrors: false
      });
    }
  };

  uploadFile = file => {
    this.getBase64(file[0]);
  };

  fileAsBase64 = (base64, file) => {
    let newFile = {
      url: "",
      name: file.name,
      object: base64
    };
    if (this._isMounted) {
      this.setState({
        file: newFile
      });
    }
  };

  getBase64 = file => {
    if (file !== undefined) {
      return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(this.fileAsBase64(reader.result, file));
        reader.onerror = error => reject(error);
      });
    }
  };

  showErrosWithField = (atributo, showErrors) =>
    !atributo.valido && showErrors ? (
      <DivShowErrors>
        <LabelShowErrors>{atributo.msgErro}</LabelShowErrors>
      </DivShowErrors>
    ) : null;

  showErrorsFile = (file, showErrors) =>
    file.url === "" && !file.object && showErrors ? (
      <DivShowErrors>
        <LabelShowErrors>Selecione um arquivo</LabelShowErrors>
      </DivShowErrors>
    ) : null;

  showErrorsPart = showErrors =>
    this.getPartsSelect().length <= 0 && showErrors ? (
      <DivShowErrors>
        <LabelShowErrors>Selecione pelo menos uma parte</LabelShowErrors>
      </DivShowErrors>
    ) : null;

  getPartsSelect = () => {
    let partsSelect = [];
    this.state.parts.forEach(part => {
      if (part.checked) {
        partsSelect.push(part.id);
      }
    });
    return partsSelect;
  };

  save = () => {
    let copia = this.state.formulario;
    let photo = this.state.file;
    let formValido = true;
    let form = [];
    for (let key in copia) {
      let atributo = ValidarRegras(copia[key], copia);
      copia[key] = atributo;
      formValido = atributo.valido ? formValido : false;
      form[key] =
        atributo.valor.value !== undefined
          ? atributo.valor.value
          : atributo.valor;
    }
    if (this._isMounted) {
      this.setState({
        formulario: copia,
        showErrors: true,
        showErroParts: true
      });
    }
    formValido = photo.url !== "" || photo.object;
    form["parts"] = this.getPartsSelect();
    formValido = form["parts"].length <= 0 ? false : formValido;
    if (formValido) {
      if (this.state.id !== undefined) {
        form["id"] = this.state.id;
      }
      form["photo"] = { ...photo };
      saveContract(form);
      this.props.history.push("/contracts");
    }
  };

  cancelar = () => {
    this.props.history.push("/contracts");
  };

  changeSelect = (checked, part) => {
    let parts = this.state.parts.map(p => {
      return p.id === part.id ? { ...p, checked: checked.target.checked } : p;
    });
    if (this._isMounted) {
      this.setState({
        parts
      });
    }
  };

  render() {
    const { formulario, showErrors, file } = this.state;
    return (
      <Template {...this.props}>
        <Title>Cadastrar/ Editar Contrato</Title>
        <ContainerForm>
          <Form>
            <Input
              id="standard-dense"
              label={formulario.titulo.title}
              value={formulario.titulo.valor}
              onChange={e => this.changeText(e.target.value, formulario.titulo)}
            />
            {this.showErrosWithField(formulario.titulo, showErrors)}
            <Input
              type="date"
              id="standard-dense"
              label={formulario.dataInicio.title}
              value={formulario.dataInicio.valor}
              onChange={e =>
                this.changeText(e.target.value, formulario.dataInicio)
              }
            />
            {this.showErrosWithField(formulario.dataInicio, showErrors)}
            <Input
              type="date"
              id="standard-dense"
              label={formulario.dataVencimento.title}
              value={formulario.dataVencimento.valor}
              onChange={e =>
                this.changeText(e.target.value, formulario.dataVencimento)
              }
            />
            {this.showErrosWithField(formulario.dataVencimento, showErrors)}
            <table style={{ width: "95%", marginTop: 20, marginLeft: 10 }}>
              <thead>
                <tr>
                  <TdTitle>Nome</TdTitle>
                  <TdTitle>Sobrenome</TdTitle>
                  <TdTitle>Email</TdTitle>
                  <TdTitle>Ações</TdTitle>
                </tr>
              </thead>
              <tbody>
                {this.state.parts.length > 0 ? (
                  this.state.parts.map(part => (
                    <tr key={part.id}>
                      <TdContent>{part.nome}</TdContent>
                      <TdContent>{part.sobrenome}</TdContent>
                      <TdContent>{part.email}</TdContent>
                      <TdContent>
                        <IconContainer>
                          <Icon>
                            <input
                              type="checkbox"
                              checked={part.checked}
                              onChange={checked =>
                                this.changeSelect(checked, part)
                              }
                            />
                          </Icon>
                        </IconContainer>
                      </TdContent>
                    </tr>
                  ))
                ) : (
                  <tr>
                    <TdContent colSpan={5}>
                      Nenhum Resultado Encontrado
                    </TdContent>
                  </tr>
                )}
              </tbody>
            </table>
            {this.showErrorsPart(showErrors)}
            <ContainerFileUpload>
              <Content>
                <Upload uploadFile={this.uploadFile} />
                {file.name !== "" ? (
                  <div style={{ textAlign: "center", marginTop: 10 }}>
                    <label> {file.name}</label>
                  </div>
                ) : null}
              </Content>
            </ContainerFileUpload>
            {this.showErrorsFile(file, showErrors)}
            <ContainerButton>
              <Button onClick={this.cancelar}>Cancelar</Button>
              <Button onClick={this.save}>Salvar</Button>
            </ContainerButton>
          </Form>
        </ContainerForm>
      </Template>
    );
  }
}

export default withRouter(ContractForm);
