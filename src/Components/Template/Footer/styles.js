import styled from 'styled-components';


export const Container = styled.footer `
    height: 150px;
    width: 100%;
    background: #74b3b6;
    display: flex;
    flex-direction: column;
    align-items: center;
    position: relative;
`