import React from "react";
import { ContainerButton, ButtomClose } from "./styles";
import Modal from 'react-modal'

export const ModalContainer = props => (
  <Modal ariaHideApp={false} style={{width: 300,height: '100%'}}isOpen={props.showModal} contentLabel="Informações do contrato">
    <ContainerButton>
      <ButtomClose onClick={props.closeModal}>
        <label style={{ marginTop: 5, marginLeft: 13 }}>X</label>
      </ButtomClose>
    </ContainerButton>
    {props.children}
  </Modal>
);
