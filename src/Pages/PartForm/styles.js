import styled from "styled-components";

export const Title = styled.div`
  display: flex;
  flex-direction: column;
  height: 40px;
  width: 100%;
  align-items: center;
  justify-content: center;
  font-size: 16px;
  font-weight: bold;
`;

export const ContainerForm = styled.div`
  width: 100%;
  height: 90%;
  padding-left: 20px;
  padding-right: 20px;
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-bottom: 20px;
`;

export const Form = styled.div`
  border-radius: 10px;
  border: 1px solid gray;
  width: 70%;
  height: 100%;
  margin-bottom: 10px;
`;

export const DivShowErrors = styled.div `
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 10px;
  height: 20px;
`

export const LabelShowErrors = styled.label `
 font-size: 14px;
  color: red;
  height: 20px;
  font-weight: bold;
`


export const ContainerButton = styled.div`
  margin-top: 30px;
  display: flex;
  justify-content:flex-end;
  width: 95%;
`;

export const Button = styled.button`
  color: #ffffff;
  background-color: #2537da;
 
  width: 200px;
  height: 40px;
  margin: 10px;
  margin-top: 50px;
  border-radius: 10px;
  &:hover {
    background: #0000ff;
  }
`;