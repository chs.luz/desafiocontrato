import React, { Component } from "react";
import Template from "../../Components/Template";
import { withRouter } from "react-router-dom";
import {
  ContainerForm,
  Title,
  IconContainer,
  Icon,
  TdTitle,
  TdContent,
  ButtomNewContract
} from "./styles";
import { MdRemove, MdEdit, MdCheck } from "react-icons/md";
import { deleteContract, listContracts } from "../../Firebase/contract";
import { ModalContainer } from "../../Components/Modal";
import { getContract } from "../../Firebase/contract";
import { getPart } from "../../Firebase/part";
import ViewContract from "../../Components/ViewContract";

class Contracts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      itens: [],
      showModal: false,
      objectCompleteContract: {
        titulo: "",
        dataInicio: "",
        dataVencimento: "",
        parts: [],
        photo: { name: "", url: "" }
      }
    };
  }

  componentDidMount() {
    this._isMounted = true;
    this.listenerContracts = listContracts(this.cbListContracts);
  }

  componentWillUnmount() {
    this._isMounted = false;
    this.listenerContracts();
  }

  cbListContracts = list => {
    if (this._isMounted) {
      this.setState({
        itens: list
      });
    }
  };

  createNewContract = () => {
    this.props.history.push("/contract-form");
  };

  delete = id => {
    deleteContract(id);
  };

  editContract = id => {
    this.props.history.push("/contract-form/" + id);
  };

  openModal = id => {
    getContract(id, this.cbGetContract);
    this.setState({
      showModal: true
    });
  };

  closeModal = () => {
    this.setState({
      showModal: false
    });
  };

  viewCompleteContract = () => {
    let objectCompleteContract = this.state.objectCompleteContract;
    return <ViewContract {...objectCompleteContract} />;
  };

  cbGetContract = contract => {
    if (this._isMounted) {
      this.setState({
        objectCompleteContract: contract
      });
    }
    if(contract.parts && contract.parts.length > 0) {
      contract.parts.forEach(part => {
        getPart(part, this.cbGetPart);
      });
    }
  };

  cbGetPart = part => {
    let objectCompleteContract = this.state.objectCompleteContract;
    objectCompleteContract.parts = objectCompleteContract.parts.map(p => {
      return p === part.id ? part : p;
    });
    if (this._isMounted) {
      this.setState({
        objectCompleteContract
      });
    }
  };
  

  render() {
    return (
      <Template {...this.props}>
        <ContainerForm>
          <ModalContainer
            showModal={this.state.showModal}
            closeModal={this.closeModal}
          >
            <ViewContract {...this.state.objectCompleteContract} />
          </ModalContainer>
          <ButtomNewContract onClick={this.createNewContract}>
            Novo
          </ButtomNewContract>
          <Title>Contratos</Title>
          <table>
            <thead>
              <tr>
                <TdTitle>ID</TdTitle>
                <TdTitle>Título</TdTitle>
                <TdTitle>Data de Início</TdTitle>
                <TdTitle>Data de Vencimento</TdTitle>
                <TdTitle>Ações</TdTitle>
              </tr>
            </thead>
            <tbody>
              {
                this.state.itens.length > 0 ?
                this.state.itens.map(item => (
                  <tr key={item.id}>
                    <TdContent>{item.id}</TdContent>
                    <TdContent>{item.titulo}</TdContent>
                    <TdContent>{item.dataInicio}</TdContent>
                    <TdContent>{item.dataVencimento}</TdContent>
                    <TdContent>
                      <IconContainer>
                        <Icon>
                          <MdCheck
                            onClick={() => this.openModal(item.id)}
                            size={20}
                            color={"blue"}
                          />
                        </Icon>
                        <Icon>
                          <MdEdit
                            onClick={() => this.editContract(item.id)}
                            size={20}
                            color={"blue"}
                          />
                        </Icon>
                        <Icon>
                          <MdRemove
                            onClick={() => this.delete(item.id)}
                            size={20}
                            color={"blue"}
                          />
                        </Icon>
                      </IconContainer>
                    </TdContent>
                  </tr>
                )):
                <tr>
                  <TdContent colSpan={5}>Nenhum Resultado Encontrado</TdContent>
                </tr>
              }
            </tbody>
          </table>
        </ContainerForm>
      </Template>
    );
  }
}

export default withRouter(Contracts);
